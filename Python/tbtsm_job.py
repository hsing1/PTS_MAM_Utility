#檢查 TBTSM_JOB 將狀態為 5 的 reset 為 0
import pyodbc
import sys
import config
import getopt

tsm_job_id = sys.argv[1]

server = config.MAM_CONFIG.db_server
database = config.MAM_CONFIG.database
username = config.MAM_CONFIG.db_user2[0]
password = config.MAM_CONFIG.db_user2[1]

#todo
def check_by_file(file_name):
    print("Check by file")

def reset_job_status(tsm_job_id):
    conn_str = 'DRIVER={{ODBC Driver 13 for SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
    cnxn = pyodbc.connect(conn_str)
    cursor = cnxn.cursor()

    update_sql = ("update TBTSM_JOB set FNSTATUS = ? where FNTSMJOB_ID = ?")
    print("Reset " + tsm_job_id + " to 0")
    cursor.execute(update_sql, 0, tsm_job_id)
    cnxn.commit()

    cnxn.close()



def check_by_tsmjob_id(tsm_job_id):
    conn_str = 'DRIVER={{ODBC Driver 13 for SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
    cnxn = pyodbc.connect(conn_str)
    cursor = cnxn.cursor()

    reset_list = []
    tsql = ("select FNTSMJOB_ID, FSSOURCE_PATH, FNSTATUS, FSNOTIFY_ADDR, FDREGISTER_TIME, FDLASTUPDATE_TIME, FSRESULT from TBTSM_JOB where FNTSMJOB_ID = ?")

    with cursor.execute(tsql, tsm_job_id):
        row = cursor.fetchone()
        while row:
            fntsmjob_id = str(row[0])
            fssource_path = str(row[1])
            fnstatus = str(row[2])
            fsnotify_addr = str(row[3])
            fdregister_time = str(row[4])
            fdlastupdate_time = str(row[5])
            fsresult = str(row[6])

            info = fntsmjob_id + "\n" + fnstatus + "\n" + fssource_path + "\n" + fsresult
            print(info)
            if (fnstatus == '5'):
                reset_list.append(fntsmjob_id)

            row = cursor.fetchone()
                
    update_sql = ("update TBTSM_JOB set FNSTATUS = ? where FNTSMJOB_ID = ?")
    for job_id in reset_list:     
        print("Reset " + job_id + " to 0")
        cursor.execute(update_sql, 0, job_id)
        cnxn.commit()

    cnxn.close()

def main(argv):
    DO_CHECK_BY_FILE = False
    DO_CHECK_BY_TSMJOB_ID = False
    DO_RESET_JOB_STATUS = False

    try:
        opts, args = getopt.getopt(argv, "", ["tsmid=", "file=", "reset="])
    except Exception as e:
        template = "Exception {0} occur : {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)

    for opt, arg in opts:
        if opt in ("--tsmid"):
            tsmjob_id = arg
            DO_CHECK_BY_TSMJOB_ID = True
        elif opt in ("--file"):
            DO_CHECK_BY_FILE = True
            file_name = arg
        elif opt in ("--reset"):
            DO_RESET_JOB_STATUS = True
            tsmjob_id = arg


    if (DO_CHECK_BY_TSMJOB_ID):
        check_by_tsmjob_id(tsmjob_id)
    elif (DO_CHECK_BY_TSMJOB_ID):
        check_by_file(file_name)
    elif (DO_RESET_JOB_STATUS):
        reset_job_status(tsmjob_id)


if (__name__  == "__main__"):
    main(sys.argv[1:])
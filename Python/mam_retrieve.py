# -*- coding: UTF-8 -*-

import os
import sys
import pyodbc
import config
import shutil
import getopt
import time
import re
from datetime import datetime
from check_file_status_win import check_by_file_no, check_by_file_list
from check_file_status_win import check_by_file_list_1, update_off_site_vol_list
from check_file_status_win import check_file_vol
from mam_archive import get_prog_archive_info
import subprocess
import ptsutils3
import smtplib
from collections import deque
import threading

DEBUG_MODE = False

DO_GET_LIBVOL_INFO = False
DO_RETRIEVE_BY_JOB_ID = False

server = config.NEWNEWS_CONFIG.db_server
database = config.NEWNEWS_CONFIG.database 
username = config.NEWNEWS_CONFIG.db_user[0]
password = config.NEWNEWS_CONFIG.db_user[1]
conn_str = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
MAMDOWNLOAD = config.MAM_STORAGE.MAMDOWNLOAD[0]
MAMDOWNLOAD_WATCH = config.MAM_STORAGE.MAMDOWNLOAD_WATCH

# Data structure for retrieving jobs
queue = deque([]) #for retrieving job information, queue[job_info1, job_info2]
job_info = [] # ['hsing1-20190223.txt', ]
thread_que = deque([])

#status = txt, queue, process, complete()
def change_retrieve_status(file_name, current_status, new_status):
    current_file = "{m}\{f}.{s}".format(m = MAMDOWNLOAD_WATCH, f = file_name, s = current_status)
    new_file = "{m}\{f}.{s}".format(m = MAMDOWNLOAD_WATCH, f = file_name, s = new_status)

    try:
        os.rename(current_file, new_file)

    except Exception as e:
        template = "Exception {0} occur : {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)
        

def get_retrieve_status(retrieve_file):
    f_initial = "{m}\{f}.{s}".format(m = MAMDOWNLOAD_WATCH, f = retrieve_file, s = "txt")
    f_queue = "{m}\{f}.{s}".format(m = MAMDOWNLOAD_WATCH, f = retrieve_file, s = "queue")
    f_process = "{m}\{f}.{s}".format(m = MAMDOWNLOAD_WATCH, f = retrieve_file, s = "process")
    f_complete = "{m}\{f}.{s}".format(m = MAMDOWNLOAD_WATCH, f = retrieve_file, s = "complete")
    check_list = [f_initial, f_queue, f_process, f_complete]
    current_status = ''
    f_name = ''

    for f in check_list:
        if (os.path.isfile(f)):
            f_name, current_status = os.path.splitext(f)
            break

    return current_status[1:]


def retrieve_by_watch_dir(watch_dir):
    thread_count = 0
    file_dict = {}
    while True:
        for r, ds, fs in os.walk(watch_dir):
            for f in fs:
                f_name, ext = os.path.splitext(f)
                if (ext == ".txt"):
                    #thread_count = threading.active_count()
                    retrieve_job = threading.Thread(target=retrieve_by_prog_file, name = f_name, args = (f_name + '.process',))
                    thread_que.append(retrieve_job)
                    change_retrieve_status(f_name, 'txt', 'queue')

        for i in range(3):
            if (len(thread_que) > 0):
                job = thread_que.popleft()
                t_name = job.getName()
                status = get_retrieve_status(t_name)
                if (status == 'queue'):
                    print("%s : queue -> process" %t_name)
                    job.start()
                    change_retrieve_status(t_name, 'queue', 'process')

        print(file_dict)

        try:
            print("Watch sleeping ... [press ctrl-C to interrupt sleeping mode]")
            time.sleep(3600)
        except KeyboardInterrupt as e:
            template = "Exception {0} occur : {1!r}"
            message = template.format(type(e).__name__, e.args)
            print(message)
            user = input("Press any kry to stop sleeping or press [n] to terminate process ...")
            if (user == 'n'):
                break
            else:
                continue


def retrieve_by_videoid(video_id, target_dir):
    HDUPLOAD_DIR = config.MAM_STORAGE.HDUPLOAD[0]
    if (len(video_id) == 8):
        video_id = video_id[2:]

    fname = video_id + '.mxf'
    file_path = os.path.join(HDUPLOAD_DIR, fname)
    if (os.path.isfile(file_path)):
        if (not os.path.exists(target_dir)):
            os.makedirs(target_dir)

        cmd = "copy \"{s}\" \"{t}\"".format(s=file_path, t=target_dir)
        print(cmd)
        os.system(cmd)
    else:
        print("File not foumd : %s" %file_path)

#--prog_file=file_name_in_watch_folder
def retrieve_by_prog_file(file_name, target_dir=None, hsm='HSM5'):
    print("DO retrieve_by_prog_file : " + file_name)
    download_dir = config.MAM_STORAGE.MAMDOWNLOAD[1] #/mnt/MAMDownload
    download_dir_win = config.MAM_STORAGE.MAMDOWNLOAD[0] #/mnt/MAMDownload
    #watch_dir = os.path.join(config.MAM_STORAGE.MAMDOWNLOAD, "WATCH")
    watch_dir = config.MAM_STORAGE.MAMDOWNLOAD_WATCH
    #archive_info = get_prog_archive_info()
    download_file = os.path.join(watch_dir, file_name)
    f_name, status = os.path.splitext(file_name)
    log_file = os.path.join(watch_dir, f_name + '.log')
    fh_log = open(log_file, "w")

    """
    if (1):
        print("%s is process ..." %file_name)
        time.sleep(30)
        print("%s is done ..." %file_name)
        change_retrieve_status(f_name, 'process', 'complete')
        return
    """

    file_name_list = os.path.splitext(file_name)
    file_name = file_name_list[0]
    user_info = file_name.split('-')
    if (len(user_info) == 2):
        user = user_info[0]
        ticket = user_info[1]
    else:
        print("Error : Invalid input file name")
        sys.exit(0)

    TARGET_DIR_WIN = os.path.join(download_dir_win, user, ticket)
    TARGET_DIR_LNX = download_dir + '/' + user + '/' + ticket
    if (not os.path.exists(TARGET_DIR_WIN)):
        os.makedirs(TARGET_DIR_WIN)

    fh = open(download_file, "r")
    lines = fh.readlines()
    retrieve_list = []
    re_file_no = re.compile(r'^[G|P|D]\d{15}$')  #need support Dxxxxxx
    re_videoid = re.compile(r'^[a-zA-Z0-9]{6,8}$')
    re_news_file_no = re.compile(r'.*(\d{15}$)')
    retrieve_file_no_list = deque([])
    msg = ""
    for l in lines:
        l = l.strip()
        if (not l): # skip blank line
            continue
        
        file_path = ptsutils3.get_file_path(l)
        if (file_path.strip()):
            retrieve_file_no_list.append(file_path)
        else:
            retrieve_list.append(l.strip())

        print(l.strip())
    
    fh.close()
        
    # convert progtam id into file path list
    tmp_list = get_file_path_by_progid_episode(retrieve_list)

    #merge file path list
    retrieve_file_no_list.extend(tmp_list)

    #pass I : scan file path for vol status
    dsmls_llist(retrieve_file_no_list)

    #pass II : scan file path for retrieving
    while True:
        if (len(retrieve_file_no_list) > 0):
            last_one = retrieve_file_no_list[-1]
        else:
            break
        
        while True:
            file_path = retrieve_file_no_list.popleft()

            print("Check %s" %file_path)
            file_info_dict = dsmls(file_path, hsm='HSM4')
            print(file_info_dict)
            file_info = next(iter(file_info_dict.values()))
            if (file_info[2] in ['o-a',  'n/a']):
                dsmrecall_to(hsm, file_path, TARGET_DIR_LNX)
            else:
                print("Not availbale %s" %file_path)
                retrieve_file_no_list.append(file_path)

            print("Remain:")
            print(retrieve_file_no_list)
            if (len(retrieve_file_no_list) == 0):
                break

            if (file_path == last_one):
                print("Retrieve %s sleeping, waiting for check in tape ..." %f_name)
                time.sleep(600)
            
    change_retrieve_status(f_name, 'process', 'complete')
    fh_log.close()


def retrieve_from_queue(retrieve_file_no_list, target):

    while True:
        if (len(retrieve_file_no_list) > 0):
            last_one = retrieve_file_no_list[-1]
        else:
            break
        
        while True:
            file_path = retrieve_file_no_list.popleft()

            print("Check %s" %file_path)
            file_info_dict = dsmls(file_path, hsm='HSM4')
            print(file_info_dict)
            file_info = next(iter(file_info_dict.values()))
            if (file_info[2] in ['o-a',  'n/a']):
                dsmrecall_to('HSM5', file_path, target)
            else:
                print("Not availbale %s" %file_path)
                retrieve_file_no_list.append(file_path)

            print("Remain:")
            print(retrieve_file_no_list)
            if (len(retrieve_file_no_list) == 0):
                break

            if (file_path == last_one):
                print("Retrieve %s sleeping, waiting for check in tape ..." %f_name)
                time.sleep(600)

# get file path by program_id, episode
def get_file_path_by_progid_episode(retrieve_list):

    re_number = re.compile(r'^[0-9]+$')
    re_range = re.compile(r'^(\d+)-(\d+)$')
    #s = '20180517 , 1, 3 , 5 , 6-8, 10-20'
    retrieve_dict = {} # {prog_id : episode_set}
    for s in retrieve_list:
        item_list = s.split(',')
        prog_id = item_list[0].strip()
    
        #get episode
        print("process %s" %prog_id)
        episode_set = set()
        if (prog_id in retrieve_dict):
            episode_set = retrieve_dict[prog_id]
            #print("Current set %s" %episode_set)

        retrieve_dict[prog_id] = episode_set

        print("process episode %s %s" %(item_list[1:], list(episode_set)))
        for item in item_list[1:]:
            item = item.strip()
            res = re_number.match(item)
            if (res is not None):       
                print(res)
                e = int(res.group(0))
                episode_set.add(e)
            
            res = re_range.match(item)
            if (res is not None):
                print(res)
                print("Range %s : %s ~ %s" %(res.group(0), res.group(1), res.group(2)))
                i = int(res.group(1))
                k = int(res.group(2))
                for e in range(i, k + 1):
                    episode_set.add(e)

        sorted(episode_set)

    file_path_list = [] 
    for prog_id in retrieve_dict:
        episode_set = retrieve_dict[prog_id]
        episode_str =  ','.join(str(e) for e in episode_set)
        tmp_list = ptsutils3.get_file_path_by_prog_id(prog_id, episode_str)
        file_path_list.extend(tmp_list)

    return file_path_list

def get_prog_dict_by_progid_episode(retrieve_list):

    re_number = re.compile(r'^[0-9]+$')
    re_range = re.compile(r'^(\d+)-(\d+)$')
    #s = '20180517 , 1, 3 , 5 , 6-8, 10-20'
    retrieve_dict = {} # {prog_id : episode_set}
    for s in retrieve_list:
        item_list = s.split(',')
        prog_id = item_list[0].strip()
    
        #get episode
        print("process %s" %prog_id)
        episode_set = set()
        if (prog_id in retrieve_dict):
            episode_set = retrieve_dict[prog_id]
            #print("Current set %s" %episode_set)

        retrieve_dict[prog_id] = episode_set

        print("process episode %s %s" %(item_list[1:], list(episode_set)))
        for item in item_list[1:]:
            item = item.strip()
            res = re_number.match(item)
            if (res is not None):       
                print(res)
                e = int(res.group(0))
                episode_set.add(e)
            
            res = re_range.match(item)
            if (res is not None):
                print(res)
                print("Range %s : %s ~ %s" %(res.group(0), res.group(1), res.group(2)))
                i = int(res.group(1))
                k = int(res.group(2))
                for e in range(i, k + 1):
                    episode_set.add(e)

        sorted(episode_set)
    
    return retrieve_dict

def retrieve_by_prog_list(prog_list, target_dir, hsm, fh_p):
    while True:
        if (len(prog_list) > 0):
            last_one = prog_list[-1]
            last_prog_id = last_one[0]
            last_episode = last_one[1]
        else:
            break

        while True:
            prog_pair = prog_list.popleft()

            retrieve_res = False
            prog_id = prog_pair[0]
            e = prog_pair[1]
            print("Retrieve %s : %s" %(prog_id, e))
            fh_p.write(prog_id + "#" + str(e) + "\n")

            #retrieve_res = True or off_site_vol[]
            retrieve_res = retrieve_by_prog_episode(prog_id, str(e), target_dir, hsm)
            if (retrieve_res != True):
                print("Waiting for check in tape ... %s" %retrieve_res)

                prog_list.append(prog_pair)

            print("Remain")
            print(prog_list)
            if (len(prog_list) == 0):
                break

            if (prog_id == last_prog_id and e == last_episode):
                time.sleep(600)
                break

                

#retrieve file from given file list
def retrieve_by_file_list(archive_info, list_file, target_dir):
    flist = check_by_file_list(list_file)
    for fno in flist:
        retrieve(archive_info, fno, target_dir)


def retrieve_by_program_id(archive_info, prog_id, episode):
    print("Retrieve by program id")

    
# file_no = P201811006930002 or G200721705720002 or 00ABCD or 0000ABCD
def retrieve(archive_info, file_no, target_dir):
    print("Retrieve file")
    re_videoid = re.compile(r'^[a-zA-Z0-9]{6,8}$')
    re_file_no = re.compile(r'^[G|P]\d{15}$')
    re_archive = re.compile(r'([G|P]\d{15})_([a-zA-Z0-9]{8})')
    video_id = ''
    fsfile_no = ''
    target_id = ''
    retrieve_status = []
    #download_dir = '\\\\10.13.200.25\\MAMDownload\\hsing1'
    
    res1 = re_videoid.match(file_no)
    res2 = re_file_no.match(file_no)
    if (res1 is not None):
        video_id = res1[0].zfill(8)
        target_id = video_id
    elif (res2 is not None):
        fsfile_no = res2[0]
        target_id = fsfile_no
    else:
        print("Error : Cannot identify file id! %s" %file_no)
    
    print("Retrieve file %s" %target_id)
    for f in archive_info:
        # get file_no and video_id
        if (target_id in f):
            path_list = archive_info[f]
            archive_path = path_list[0]
            stud_file = archive_path +  '.stud'
            if (os.path.isfile(archive_path)):
                cmd = "copy \"{s}\" \"{t}\"".format(s=archive_path, t=target_dir)
                os.system(cmd)
                FOUND = True
                break
            else:
                print("%s not found!" %archive_path)

def watch_vol_status(flist, hsm='HSM4'):
    print("DO watch_vol_sttus")
    while True:
        try:
            dsmls_list(flist, hsm)
            print("Sleeping ... [press ctrl-C to interrupt sleeping mode]")
            time.sleep(3600)
        except KeyboardInterrupt as e:
            template = "Exception {0} occur : {1!r}"
            message = template.format(type(e).__name__, e.args)
            print(message)
            user = input("Press any kry to stop sleeping or press [n] to terminate process ...")
            if (user == 'n'):
                break
            else:
                continue

def dsmls_llist(plist, hsm='HSM4'):
    print("Do dsmls_llist")
    status_dict = {}
    for l in plist:
        status_dict.update(dsmls(l, hsm))

def dsmls_list(flist, hsm='HSM4'):
    print("DO dsmls_list")
    fh = open(flist, "r")
    lines = fh.readlines()
    status_dict = {}
    for l in lines:
        fpath = l.rstrip()
        if (not fpath):
            continue #blank line

        status_dict.update(dsmls(fpath, hsm))
    
    print(status_dict)

#todo:support video_id
#support file_no
def dsmls(fpath, hsm='HSM4'):
    print("DO dsmls")
    HSM = config.HSM_CONFIG.HSM[hsm]
    login = config.HSM_CONFIG.login
    passwd = config.HSM_CONFIG.password
    tsm_login = config.TSM_CONFIG.tsm_user1[0]
    tsm_passwd = config.TSM_CONFIG.tsm_user1[1]
    tmp_path = ptsutils3.get_file_path(fpath)
    tsm_path = ptsutils3.mampath2tsmpath(tmp_path)
    vol_status = ''

    #if (not fpath.strip()):
    #    return None

    #print(tsm_path)
    print("dsmls '%s'" %tsm_path)

    plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
    dsmls_args = ['dsmls', tsm_path, '| sed 1,8d']
    plink_args.extend(dsmls_args)
    proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
    lines = proc.stdout.readlines()
    rc = proc.returncode
    exit_code = proc.wait()
    #print(exit_code) 0 : success
    #print("Return code %i" %rc)
    file_info_dict = {}
    for  l in lines:
        print(l) #=> b'  Client Version 6, Release 3, Level 0.0  \n'
        l = l.decode('utf-8')
        #print(l)
        res = l.split()
        #print(res)
        file_size = int(res[0])
        file_status = res[3] #p, r, m
        vol_status = ''  #off-a, off-u, o-a, o-u
        file_info_list = [file_size, file_status]
        file_res = os.path.splitext(res[4])
        file_no = file_res[0]
        file_info_dict[file_no] = file_info_list
        #print(file_no)
        if (file_status == 'p'):
            print("File %s is online (migrated)" %file_no)
            vol_status = 'o-a'
        elif (file_status == 'r'):
            print("File %s is online (not migrated)" %file_no)
            vol_status = 'n/a'
        elif (file_status == 'm'): #need check tape volume status
            print("File %s is migrated" %file_no)
            vol_info_list = check_by_file_no(file_no)  # will update all off-site vol status
            status_dict = vol_info_list[1]
            for v in status_dict.keys():
                vol_status = status_dict[v]
                if (vol_status == 'off-a'):
                    print("%s is off-site, available" %v)
                elif (vol_status == 'off-u'):
                    print("%s is off-site, unavailable" %v)
                elif (vol_status == 'o-a'):
                    print("%s is on-site, available" %v)
                    print("Do dsmrecall")
                    #dsmrecall(tsm_path)
                elif (vol_status == 'o-u'):
                    print("%s is on-site, unavailable" %v)
        file_info_list.append(vol_status)

    #pass empty set for updating tape status
    s = set()
    #update_off_site_vol_list(s) already update in check_by_file_no
    return file_info_dict


# recall by file list, and copy to target dir
def dsmrecall_list_to(hsm, flist, target_dir):
    fh = open(flist, "r")
    lines = fh.readlines()
    for l in lines:
        fpath = l.rstrip()
        dsmrecall_to(hsm, fpath, target_dir)


#recall and copy to target folder 
def dsmrecall_to(hsm, fpath, target_dir):
    print("DO dsmrecall_to")
    HSM = config.HSM_CONFIG.HSM[hsm]
    HSM4 = config.HSM_CONFIG.HSM['HSM4']
    login = config.HSM_CONFIG.login
    passwd = config.HSM_CONFIG.password
    tsm_login = config.TSM_CONFIG.tsm_user1[0]
    tsm_passwd = config.TSM_CONFIG.tsm_user1[1]
    tsm_path = ptsutils3.mampath2tsmpath(fpath)
    DO_COPY_FILE = False

    plink_args = ['plink', '-ssh', HSM4, '-l', login, '-pw', passwd] 

    file_info = dsmls(fpath, hsm)
    file_name = ''
    file_size = 0
    migrate_status = ''
    w = os.path.abspath(target_dir)
    res = w.split('MAMDownload')
    if (len(res) == 2): # to MAMDownload
        target_dir_win = config.MAM_STORAGE.MAMDOWNLOAD[0] + res[1]

    res = w.split('Retrieve')
    if (len(res) == 2): # to Retrieve
        target_dir_win = config.MAM_STORAGE.RETRIEVE[0] + res[1]

    target_file_win = ''
    for f in file_info:
        file_name = f + '.mxf'
        org_file_size = file_info[f][0]
        migrate_status = file_info[f][1]
        #target_file = os.path.join(target_dir, file_name)

        target_file_win = target_dir_win + '\\' + file_name  #use linux path for dsmrecall
        target_file_lnx = target_dir + '/' + file_name  #use linux path for dsmrecall
    
    if (os.path.isfile(target_file_win)):
        statInfo = os.stat(target_file_win)
        target_size = statInfo.st_size
        if (target_size != org_file_size):
            DO_COPY_FILE = True
    else:
        DO_COPY_FILE = True

    if (DO_COPY_FILE == False):
        print("File %s already exist!" %target_file_win)
        return

    print("recall %s on %s, copy to %s" %(tsm_path, hsm, target_dir))

    plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
    dsmrecall_args = ['dsmrecall', tsm_path, ';']
    copy_args = ['cp', tsm_path,  target_dir, '-v']
    plink_args.extend(dsmrecall_args)
    plink_args.extend(copy_args)
    proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
    lines = proc.stdout.readlines()
    for l in lines:
        print(l.decode('utf-8').rstrip())

def dsmrecall_list(hsm, flist):
    fh = open(flist, "r")
    lines = fh.readlines()
    for l in lines:
        fpath = l.rstrip()
        dsmrecall(hsm, fpath)


def dsmrecall(hsm, fpath):
    print("Do dsmrecall")
    HSM = config.HSM_CONFIG.HSM[hsm]
    login = config.HSM_CONFIG.login
    passwd = config.HSM_CONFIG.password
    tsm_login = config.TSM_CONFIG.tsm_user1[0]
    tsm_passwd = config.TSM_CONFIG.tsm_user1[1]
    tsm_path = ptsutils3.mampath2tsmpath(fpath)

    print("recall %s on %s" %(tsm_path, hsm))

    plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
    dsmrecall_args = ['dsmrecall', tsm_path]
    plink_args.extend(dsmrecall_args)
    proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
    lines = proc.stdout.readlines()
    for l in lines:
        print(l.decode('utf-8').rstrip())

def get_file_no_by_progid(prog_id, episodes = None):
    print("DO get_file_no_by_progid")
    server = config.MAM_CONFIG.db_server
    database = config.MAM_CONFIG.database
    username = config.MAM_CONFIG.db_user1[0]
    password = config.MAM_CONFIG.db_user1[1]
    connStr = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
    #tsql = ("select FSFILE_NO, FSFILE_PATH_H, FSVIDEO_PROG from TBLOG_VIDEO where FSID = ? and FNEPISODE = ? and FSARC_TYPE in ('001', '002') and FCFILE_STATUS = 'T'")
    tsql = "select FSFILE_NO, FSFILE_PATH_H, FSVIDEO_PROG from TBLOG_VIDEO where FSID = ? and FNEPISODE in ("  + episodes  + ") and FSARC_TYPE in ('001', '002') and FCFILE_STATUS = 'T'"
    cnxn = pyodbc.connect(connStr)
    cursor = cnxn.cursor()

    retrieve_dict = {}
    with cursor.execute(tsql, prog_id):
        row = cursor.fetchone()
        while row:
            file_vol_status = []
            file_no = str(row[0])
            file_path_h = str(row[1])
            video_id = str(row[2])
            fkey = file_no + '_' + video_id
            file_vol_status.append(file_path_h)
            retrieve_dict[fkey] = file_vol_status 
            row = cursor.fetchone()
    cnxn.close()

    #if (len(retrieve_dict) > 1):
    #    print("Error")

    return retrieve_dict

def retrieve_by_prog_episode(prog_id, episode, target_dir=None, hsm='HSM5'):
    log_msg = "DO {0}".format("retrieve_by_prog_episode")
    print(log_msg)
    archive_info = get_prog_archive_info()
    RETRIEVED = False
    print("Retrieve by program id and episode (1 episode)")
    #get {fileno_videoid : file_status_list}
    ret_dict = get_file_no_by_progid(prog_id, episode)
    if (DEBUG_MODE):
        print(ret_dict)

    total_file = len(ret_dict)
    retrieve_count = 0
    template = "{0} retrieve({1})"
    timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
    msg = template.format(timestamp, retrieve_count)
    print(msg)

    if (DEBUG_MODE):
        print(ret_dict)

    for f in ret_dict:
        file_info_list = ret_dict[f] #[STATUS, status_dict]
        if (len(file_info_list) > 1):
            print("Remove vol status from file info list")
            del file_info_list[1:]

        path_list = archive_info[f]
        #status_list = check_by_file_list_1(retrieve_list)
        vol_status_list = check_by_file_no(path_list[0])
        file_info_list.extend(vol_status_list)

    if (DEBUG_MODE):
        print(ret_dict)

    print("Begin to retrieve files ...")
    for f in ret_dict:
        file_info_list = ret_dict[f]
        archive_path = file_info_list[0]
        vol_status = file_info_list[1]
        if (vol_status == 'AVAILABLE'):
            if (target_dir):
                dsmrecall_to(hsm, archive_path, target_dir)
            else:
                dsmrecall(hsm, archive_path)
            #del ret_dict[f]
            retrieve_count += 1
            RETRIEVED = True
            return RETRIEVED
        elif (vol_status == 'NONE'): # could be filling
            if (target_dir):
                dsmrecall_to(hsm, archive_path, target_dir)
            else:
                dsmrecall(hsm, archive_path)
            #del ret_dict[f]
            retrieve_count += 1
            RETRIEVED = True
            return RETRIEVED
        else: #OFF-SITE,AVAILABLE, OFF-SITE,UNAVAILABLE
            off_site_vol = []
            vol_status_dict = file_info_list[2]
            for vol in vol_status_dict:
                print("%s : %s" %(vol, vol_status_dict[vol]))
                vstatus = vol_status_dict[vol]
                if (vstatus in ('off-a', 'off-u')):
                    off_site_vol.append(vol)
            return off_site_vol


#todo: implement log
#--prog=2007217 --episode=507,508,509 [--to=/mnt/MAMDownload/user/201812210001]
def retrieve_by_prog_episodes(prog_id, episodes, target_dir=None, hsm='HSM4'):
    log_msg = "DO {0}".format("retrieve_by_prog_episodes")
    print(log_msg)
    archive_info = get_prog_archive_info()
    print("Retrieve by program id and episodes")
    #get [fileno_videoid : file_status_list]
    ret_dict = get_file_no_by_progid(prog_id, episodes)
    if (DEBUG_MODE):
        print(ret_dict)

    while True:
        total_file = len(ret_dict)
        retrieve_count = 0
        template = "{0} retrieve {1}"
        timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
        msg = template.format(timestamp, retrieve_count)
        print(msg)

        if (DEBUG_MODE):
            print(ret_dict)

        for f in ret_dict:
            file_info_list = ret_dict[f] #[STATUS, status_dict]
            if (len(file_info_list) > 1):
                print("Remove vol status from file info list")
                del file_info_list[1:]

            path_list = archive_info[f]
            #status_list = check_by_file_list_1(retrieve_list)
            vol_status_list = check_by_file_no(path_list[0])
            file_info_list.extend(vol_status_list)

        if (DEBUG_MODE):
            print(ret_dict)

        print("Begin to retrieve files ...")

        for f in ret_dict:
            file_info_list = ret_dict[f]
            archive_path = file_info_list[0]
            vol_status = file_info_list[1]
            if (vol_status == 'AVAILABLE'):
                if (target_dir):
                    dsmrecall_to(hsm, archive_path, target_dir)
                else:
                    dsmrecall(hsm, archive_path)
                #del ret_dict[f]
                retrieve_count += 1
            else:
                vol_status_dict = file_info_list[2]
                for vol in vol_status_dict:
                    print("%s : %s" %(vol, vol_status_dict[vol]))

        if (DEBUG_MODE):
            print(retrieve_count)
            print(total_file)

        if (retrieve_count == total_file):
            break

        print("Waiting for check in tape") 
        time.sleep(600)


def get_retrieve_list(retrieve_list):
    fh = open(retrieve_list, 'r')
    lines = fh.readlines()
    retrieve_list = []
    re_number = re.compile(r'^[0-9]?$')
    re_range = re.compile(r'^(\d+)-(\d+)$')
    for l in lines:
        retrieve_list.append(l.strip())
        s = retrieve_list[0]
        
    for s in retrieve_list:
        #s = '20180517 , 1, 3 , 5 , 6-8, 10-20'
        item_list = s.split(',')
    
        new_item_list = []
        for item in item_list:
            new_item_list.append(item.strip())
        print(new_item_list)
    
        # get program idhsm, 
        prog_id = item_list[0].strip()

        # get episode number
        episode_list = []
        for item in item_list[1:]:
            item = item.strip()
            res = re_number.match(item)
            if (res is not None):       
                print(res)
                episode_list.append(int(res.group(0)))
            
            res = re_range.match(item)
            if (res is not None):
                print(res)
                print("Range %s : %s ~ %s" %(res.group(0), res.group(1), res.group(2)))
                i = int(res.group(1))
                k = int(res.group(2))
                for e in range(i, k + 1):
                    episode_list.append(e)
            
        print(episode_list)
    
def recall_to_approved(play_out_file): #HD_FILE_MANAGER GPFS to Approved
    log_msg = "DO {0}".format("recall_to_approved")
    print(log_msg)

    HSM = config.HSM_CONFIG.HSM['HSM5']
    login = config.HSM_CONFIG.login
    passwd = config.HSM_CONFIG.password
    tsm_login = config.TSM_CONFIG.tsm_user1[0]
    tsm_passwd = config.TSM_CONFIG.tsm_user1[1]
    fh = open(play_out_file, 'r', encoding='utf-8')
    lines = fh.readlines()
    for l in lines:
        l = l.strip()
        item_list = l.split()
        video_id = item_list[0]
        samba_file = item_list[1]
        tsm_path = ptsutils3.mampath2tsmpath(samba_file)
        tsm_info_dict = dsmls(tsm_path, 'HSM4')
        file_info = next(iter(tsm_info_dict.values()))
        if (file_info[2] in ['o-a',  'n/a']):
            #dsmrecall('HSM5', tsm_path)
            target = "/mnt/mamnas1/Approved/" + video_id + ".mxf"
            msg = "copy {s} to {t}".format(s=tsm_path, t=target)
            print(msg)

            plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
            dsmrecall_args = ['dsmrecall', tsm_path, ';']
            #copy_args = ['cp', tsm_path,  target, '-vf']
            copy_args = "cp {s} {t} -vf".format(s=tsm_path,  t=target)
            plink_args.extend(dsmrecall_args)
            plink_cmd = ' '.join(plink_args)
            #plink_args.extend(copy_args)
            cmd = "{p} {c}".format(p=plink_cmd, c=copy_args)
            #print(plink_args)
            proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
            lines = proc.stdout.readlines()
            for l in lines:
                print(l.decode('utf-8').rstrip())

            ptsutils3.create_louth_db(video_id)
        else:
            print("Not availbale %s" %tsm_path)

def retrieve_by_jobid(job_id):
    print("DO retrieve_by_jobid")
    target_root = config.MAM_STORAGE.MAMDOWNLOAD[1]
    server = config.MAM_CONFIG.db_server
    database = config.MAM_CONFIG.database
    username = config.MAM_CONFIG.db_user1[0]
    password = config.MAM_CONFIG.db_user1[1]
    connStr = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
    sp = "SP_Q_GET_BOOKING_FILE_BY_BOOKINGNO ?"
    cnxn = pyodbc.connect(connStr)
    cursor = cnxn.cursor()

    #jobs = {job_id : job_info}
    #job_info = [user, file1, file2, ...]
    jobs = {}
    job_info = []
    jobs[job_id] = job_info 
    file_list = deque([])
    fsuser = ''
    target = ''
    exist = False
    with cursor.execute(sp, job_id):
        row = cursor.fetchone()
        while row:
            #fsuser_id = str(row[2])
            if (fsuser == ''):
                fsuser = str(row[3])
                target = "{0}/{1}/{2}".format(target_root, fsuser, job_id)

            file_no = str(row[11])
            exist = ptsutils3.check_file_in_mamdownload(file_no, target)
            if (not exist):
                file_path = ptsutils3.get_file_path(file_no)
                file_list.append(file_path)
                print("{0} doesn't exist!".format(file_no))

            row = cursor.fetchone()
    cnxn.close()

    job_info.append(fsuser)
    job_info.append(file_list)

    #print(job_info)

    #pass I : scan file path for vol status
    dsmls_llist(file_list)

    #pass II : scan file path for retrieving
    retrieve_from_queue(file_list, target)



def send_email():
    smtpObj = smtplib.SMTP('smtp.gmail.com', 587)
    print(type(smtpObj))
    print(smtpObj.starttls())

    smtpObj = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    print(type(smtpObj))
    smtpObj.ehlo()

    #login
    res = smtpObj.login('taiwancycling@gmail.com', 'evercad1')
    print(res)
    smtpObj.sendmail('pts.hsing1@gmail.com', 'hsing1@mail.pts.org.tw',  
        'Subject: So long.\nDear Alice, so long and thanks for all the fish. Sincerely, Bob')
    smtpObj.quit()
    
def usage():
    usage_str = """
NAME
    mam_retrieve - Retrieve file from GPFS, ISILON, HDUPLOAD etc

SYNOPSIS
    mam_retrieve [OPTION]

DESCRIPTION

    --dsmls=TSM_PATH
        check TSM file status. For multiple files, use --fdsmls
        ex: /ptstv/HVideo/2018/20180600624/P201806006240001.mxf or
            \\\\mamstream\\MAMDFS\\ptstv\\HVideo\\2018\\20180600621\P201806006210001.mxf

    --dsmrecall=FILE_PATH
        to recall file from TSM(GPFS). For multiple files, use --fdsmrecall
        ex: /ptstv/HVideo/2018/20180600624/P201806006240001.mxf or
            \\\\mamstream\\MAMDFS\\ptstv\\HVideo\\2018\\20180600621\P201806006210001.mxf

    --episode=EPISODE_LIST
        specify episode number to be retrieved, ex:1,2,3

    --fdsmls=FILE
        file list contain file path to check dsmls status. For single file, use --dsmls.
        with --watch option for continuous checking file status
    
    --get_vol=file_no
        get volume number by specifing file number
        ex:
        --get_vol=G201510700040008
        --get_vol=P201903007750002
        --get_vol=201609020010095  (for news)

    --get_vol_status=file_no
        get volume status by specifing file number
        the voulme status will update to \\\\10.13.200.25\\MAMDownload\\tsm_off_site_vol.txt
    
    --hsm=HSM_SERVER_NAME
        specify hsm to execute command. ex HSM1, HSM2, HSM3, HSM4(default), HSM5

    --prog_file=DOWNLOAD_FILE
        place program id and episodes in download_file

    --prog=PROGRAM_ID
        specify program id to be retrieved

    --to=TARGET_DIR
        copy recalled file to TARGET_DIR. Use --dsmrecall or --fdsmrecall to recall files.

    --video_id=VIDEO_ID
        retrieve by video id, will search file in HDUpload ...
        for non-archived file

    --watch_vol_status      
        enable watch mode to check volume status, and update status in monitor file
        combine with --fdsmls option
        ex:
        --fdsmls=file_path.txt --watch_vol_status
    
    --watch_dir=WATCH_DIR
        specify watch folder WATCH_DIR, it contain file list for retrieving

    -r, --retrieve : retrieve by file number
    --rfile=retrive_file : retrieve by file list
    """
    print(usage_str)

def main(argv):
    global DO_RETRIEVE_BY_JOB_ID
    global DO_GET_LIBVOL_INFO

    DO_RETRIEVE_BY_PROG_ID = False
    DO_RETRIEVE_FILE = False
    DO_RETRIEVE_BY_EPISODE = False
    RETRIEVE_MODE = False
    DO_RETRIEVE_LIST_MODE = False
    DO_DSMLS = False
    DO_DSMLS_LIST = False
    DO_DSMRECALL = False
    DO_DSMRECALL_TO = False
    DO_DSMRECALL_LIST = False
    DO_RETRIEVE_BY_PROG_FILE = False
    DO_WATCH_VOL_STATUS = False
    DO_RETRIEVE_VIDEOID = False
    DO_GET_PATH = False
    DO_GET_VOLUME = False
    DO_GET_VOLUME_INFO = False
    DO_GET_VOLUME_STATUS = False
    DO_RETRIEVE_BY_WATCH_DIR = False
    DO_RECALL_TO_APPROVED = False
    DO_GET_LIBVOL = False
    DO_DUMP_VOL_BY_STGPOOL = False

    file_no = ''
    watch_dir = ''
    target_dir = ''
    hsm = 'HSM5' #default
    watch_retrieve_dir = ''
    global DEBUG_MODE

    try:
        opts, args = getopt.getopt(argv, "hd:f:r:w:t:", ["fdsmls=", "dsmls=", "dir=", "target_dir=", "file="
        , "retrieve_job=" #specify retrieve job id
        , "retrieve=", "rfile=", "tcase=", "prog=", "episode="
        , "dsmrecall=", "hsm=", "to=", "fdsmrecall="
        , "prog_file=", "watch_vol_status", "debug"
        , "stg_file="
        , "video_id="
        , "get_path="
        , "get_vol=", "get_vol_status=", "get_libvol=", "get_vol_by_stgp="
        , "get_libvol_info=", "stgp=", "vol_type=", "max_vol="
        , "watch_dir=", "gpfs2approved="])

    except Exception as e:
        template = "Exception {0} occur : {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)

        sys.exit(2)

    for opt, arg in opts:
        print("Process opt %s, %s" %(opt, arg))
        if opt in ("-d", "--dir"):
            DO_RETRIEVE_DIR = True
            retrieve_dir = arg 
        elif (opt == "--debug"):
            DEBUG_MODE = True
            print("BEGIN DEBUG MODE")
        elif opt in ("--dsmls"):
            DO_DSMLS = True
            tsm_path = arg
        elif opt in ("--dsmrecall"):
            DO_DSMRECALL = True
            tsm_path = arg
        if opt in ("--episode"): #specify episode
            DO_RETRIEVE_BY_EPISODE = True
            episode = arg
        elif opt in ("-f", "--file"): # specify file to retrieve
            DO_RETRIEVE_FILE = True
            file_path = arg
        elif opt in ("--fdsmls"):
            DO_DSMLS_LIST = True
            flist = arg
        elif opt in ("--fdsmrecall"):
            DO_DSMRECALL_LIST = True
            flist = arg
        elif (opt == "--get_path"):
            DO_GET_PATH = True
            file_no = arg
        elif (opt == "--get_vol"): #get volumn name of file
            DO_GET_VOLUME = True
            file_no = arg
        elif (opt == "--get_vol_status"):
            DO_GET_VOLUME_STATUS = True
            file_no = arg
        elif (opt == "--get_libvol"):
            DO_GET_LIBVOL = True
            libname = arg
        elif (opt == "--get_libvol_info"):
            DO_GET_LIBVOL_INFO = True
            libname = arg
        elif opt in ("--gpfs2approved"): #recall and copy file to Approved
            DO_RECALL_TO_APPROVED = True
            play_out_file = arg
        elif (opt == "--get_vol_by_stgp"): #need --stg_file
            DO_DUMP_VOL_BY_STGPOOL = True
            stgp = arg #PTSTVHSM, HAKKATVHSM ...
        elif (opt == '-h'):
            usage()
            sys.exit()
        elif opt in ("--hsm"):
            hsm = arg.upper()
        elif (opt == "--max_vol"): #16
            max_vol = int(arg)
        elif (opt == "--prog_file"):
            DO_RETRIEVE_BY_PROG_FILE = True
            file_name = arg
        elif (opt == "--prog"): #retrieve by program id and episode
            DO_RETRIEVE_BY_PROG_ID = True
            prog_id = arg
        elif opt in ("-r", "--retrieve"):
            file_no = arg
            RETRIEVE_MODE = True
        elif (opt == "--retrieve_job"): #specify retrieve job id
            rjob_id = arg
            DO_RETRIEVE_BY_JOBID = True
        elif opt in ("--rfile"):
            retrieve_file = arg
            DO_RETRIEVE_LIST_MODE = True
        elif (opt == "--stgp"): #specify stgpool name, for --get_libbol_info
            stgp = arg #PTSTVHSM, HAKKATVHSM ...
        elif (opt == "--stg_file"):
            file_name = arg #any filename
        elif opt in ("-t", "--target_dir"): # retrieve to target dir
            target_dir = arg
        elif opt in ("--tcase"):
            test_case(int(arg))
            sys.exit(2)
        elif opt in ("--to"):
            DO_DSMRECALL_TO = True
            target_dir = arg
        elif (opt == "--video_id"):
            DO_RETRIEVE_VIDEOID = True
            video_id = arg
        elif (opt == "--vol_type"): #Full(0) | Filling(1) | All(2)
            vol_type  = int(arg)
        elif opt in ("--watch_vol_status"):
            DO_WATCH_VOL_STATUS = True
        elif (opt == "--watch_dir"):
            DO_RETRIEVE_BY_WATCH_DIR = True
            watch_retrieve_dir = arg

    # query libvol TS3500/TS3310
    if (DO_GET_LIBVOL):
        vol_list = ptsutils3.get_libvol(libname)
        for v in vol_list:
            print(v)

    #--get_libvol_info=ts3500 --stgp=PTSTVHSM --vol_type=0|1,2 
    if (DO_GET_LIBVOL_INFO):
        ptsutils3.dump_libvol_info(libname, stgp, vol_type, max_vol)

    if (DO_DUMP_VOL_BY_STGPOOL):
        ptsutils3.dump_stgp_vol_info(stgp, file_name)
        vol_list = ptsutils3.get_vol_name(file_name, 1)
        for v in vol_list:
            print(v)

    if (DO_RECALL_TO_APPROVED):
        recall_to_approved(play_out_file)
    
    if (RETRIEVE_MODE):
        archive_info = get_prog_archive_info()
        retrieve(archive_info, file_no, target_dir)

    elif (DO_RETRIEVE_BY_PROG_FILE):
        retrieve_by_prog_file(file_name, None , hsm)

    elif (DO_RETRIEVE_BY_PROG_ID and DO_RETRIEVE_BY_EPISODE): #--prog, --episode [--hsm, --to]
        retrieve_by_prog_episodes(prog_id, episode, target_dir, hsm)

    elif (DO_RETRIEVE_LIST_MODE):
        archive_info = get_prog_archive_info()
        retrieve_by_file_list(archive_info, retrieve_file, target_dir)

    elif (DO_RETRIEVE_BY_JOBID):
        retrieve_by_jobid(rjob_id)

    elif (DO_DSMLS):
        dsmls(tsm_path, hsm)
    
    elif (DO_DSMLS_LIST and not DO_WATCH_VOL_STATUS): #--fdsmls=
        dsmls_list(flist, hsm)
    
    elif (DO_DSMRECALL and not DO_DSMRECALL_TO):
        dsmrecall(hsm, tsm_path)
    
    elif (DO_DSMRECALL and DO_DSMRECALL_TO):
        dsmrecall_to(hsm, tsm_path, target_dir)
    
    elif (DO_DSMRECALL_LIST and DO_DSMRECALL_TO):
        dsmrecall_list_to(hsm, flist, target_dir)

    elif (DO_DSMRECALL_LIST and not DO_DSMRECALL_TO): #--fdsmrecall= -recallto=
        dsmrecall_list(hsm, flist)

    elif (DO_WATCH_VOL_STATUS and DO_DSMLS_LIST):
        watch_vol_status(flist)

    elif (DO_RETRIEVE_VIDEOID and DO_DSMRECALL_TO):
        retrieve_by_videoid(video_id, target_dir)
    
    elif (DO_GET_PATH):
        file_path = ptsutils3.get_file_path(file_no)
        if (file_path):
            print(file_path)
        else:
            print("Cannot find " + file_no)

    elif (DO_GET_VOLUME):
        vol_set = check_file_vol(file_no)
        print("Volume = " + str(vol_set))

    elif (DO_GET_VOLUME_STATUS):
        check_by_file_no(file_no)

    elif (DO_RETRIEVE_BY_WATCH_DIR):
        retrieve_by_watch_dir(watch_retrieve_dir)


def test_case(case_num):
    archive_info = {}
    TARGET_DIR = '\\\\isilon.pts.org.tw\MAM_ARCHIVE'
    if (case_num == 1):
        #archive_info = get_prog_archive_info()

        fh = open("E:\\TEMP\\51204-20181208.txt", 'r')
        lines = fh.readlines()
        retrieve_list = []
        for l in lines:
            retrieve_list.append(l.strip())
            print(l.strip())
            
        re_number = re.compile(r'^[0-9]?$')
        re_range = re.compile(r'^(\d+)-(\d+)$')
        #s = '20180517 , 1, 3 , 5 , 6-8, 10-20'
        s = retrieve_list[0]
        item_list = s.split(',')
        print(item_list)
        
        new_item_list = []
        for item in item_list:
            new_item_list.append(item.strip())
        print(new_item_list)
        
        episode_list = []
        prog_id = item_list[0].strip()
        for item in item_list[1:]:
            item = item.strip()
            res = re_number.match(item)
            if (res is not None):       
                print(res)
                episode_list.append(int(res.group(0)))
                
            res = re_range.match(item)
            if (res is not None):
                print(res)
                print("Range %s : %s ~ %s" %(res.group(0), res.group(1), res.group(2)))
                i = int(res.group(1))
                k = int(res.group(2))
                for e in range(i, k + 1):
                    episode_list.append(e)
                
        print(episode_list)

    if (case_num == 2):
        server = config.MAM_CONFIG.db_server
        database = config.MAM_CONFIG.database
        username = config.MAM_CONFIG.db_user1[0]
        password = config.MAM_CONFIG.db_user1[1]
        connStr = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
        cnxn = pyodbc.connect(connStr)
        cursor = cnxn.cursor()

        l = ['507', '508', '509']
        prog_id = '2007217'
        #episodes = '(507, 508, 509)'
        episodes = '(' + ','.join(l) + ')'
        tsql = 'select FSFILE_NO, FSFILE_PATH_H, FSVIDEO_PROG from TBLOG_VIDEO where FSID = ? and FNEPISODE in ' + episodes +  ' and FSARC_TYPE in (\'001\', \'002\') and FCFILE_STATUS = \'T\''
        episode = '507'
        with cursor.execute(tsql, prog_id):
            row = cursor.fetchone()
            while row:
                file_no = str(row[0])
                file_path = str(row[1])
                print("%s : %s" %(file_no, file_path))
                row = cursor.fetchone()

        cnxn.close()

    if (case_num == 3):
        send_email()

    if (case_num == 4):
        #f1 =  "E:\\TSM\\TITV_Recycle\\before.txt"
        #f2 =  "E:\\TSM\\TITV_Recycle\\after.txt"
        f1 = "C:\Repository\DevOpsUtilsTest\Python\scratch.txt"
        f2 = "C:\Repository\DevOpsUtilsTest\Python\scratch1.txt"
        ptsutils3.diff_file(f1, f2)
        
    
if (__name__ == "__main__"):
    main(sys.argv[1:])
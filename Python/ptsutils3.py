#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os.path
import os
import sys
import pyodbc
import sqlite3
import shutil
import re
import subprocess
import time
import config
import getopt

hsm_login = config.HSM_CONFIG.login
hsm_passwd =  config.HSM_CONFIG.password
tsm_login = config.TSM_CONFIG.tsm_user1[0] 
tsm_passwd = config.TSM_CONFIG.tsm_user1[1]
hsm = config.HSM_CONFIG.HSM['HSM4']
LOUTH_DB_GENERATE = '.\LOUTH-DB-Create.exe'

plink_args = 'plink -ssh {hsm} -l {login} -pw {passwd}'.format(hsm = hsm, login = hsm_login, passwd = hsm_passwd)
dsmadmc_args = 'dsmadmc -id={login} -password={passwd}'.format(login = tsm_login, passwd = tsm_passwd)
connection = '{plink} {dsmadmc}'.format(plink = plink_args, dsmadmc = dsmadmc_args)

#check if file exist in MAMDownload
def check_file_in_mamdownload(file_no, target_dir):
    find = False
    ls_cmd = "ls {p}/*{f}*".format(p = target_dir, f = file_no)
    cmd = "{0} {1}".format(plink_args, ls_cmd)
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    lines = proc.stdout.readlines()
    exit_code = proc.wait()
    for l in lines:
        l = l.decode('utf-8')
        res = l.strip()
        print(res)
        find = True

    return find

def get_news_file_path(file_no):
    TARGET_ROOT = ["/news/pts", "/news/hakka", "/news/titv"]
    file_path = ''

    reExt = re.compile(".*\.mxf$", re.IGNORECASE)

    year = file_no[0:4]
    mon = file_no[4:6]
    day = file_no[6:8]

        #check if file already in GPFS
    for r in TARGET_ROOT:
        target = "{r}/{y}/{m}/{d}/{f_dir}/{f}.*".format(r = r, y = year, m = mon, d = day, f_dir = file_no, f = file_no)
        #print("check: " + target)
        linux_args = "ls {p}".format(p = target)
        cmd = "{plink} {linux}".format(plink = plink_args, linux = linux_args)
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        lines = proc.stdout.readlines()
        exit_code = proc.wait()
        if (exit_code == 0):
            #print("Found file " + target)
            for l in lines:
                l = l.decode('utf-8')
                res = l.strip()
                #print(res)
                file_path = res
                #print("Found file " + res)
            break
        

    return file_path

# check if file exist
def check_file_exist(plink_login, file_path):

    linux_args = "ls {p}".format(p = file_path)
    cmd = "{plink} {linux}".format(plink = plink_args, linux = linux_args)
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    lines = proc.stdout.readlines()
    exit_code = proc.wait()
    if (exit_code == 0):
        #print("Found file " + target)
        for l in lines:
            l = l.decode('utf-8')
            res = l.strip()
            #print(res)
            file_path = res
            #print("Found file " + res)

    return file_path

def check_file_exist_by_list(in_file):
    fh = open(in_file, "r")
    fho = open("file_not_found.txt", "w")
    lines = fh.readlines()
    
    for p in lines:
        p = p.rstrip()
        print("check : " + p)
        if (p == 'NULL'):
            continue

        #fho.writelines("check : " + p + "\n")
        if (not os.path.isfile(p)):
            print("File not found : " + p)
            fho.writelines(p + "\n")

def diff_file(f1, f2):
    fh1 = open(f1, "r")
    fh2 = open(f2, "r")
    #fo = open("scratch.txt", "w", encoding='utf-8')

    line1 = fh1.readlines()
    line2 = fh2.readlines()

    for l1 in line1:
        if (l1 not in (line2)):
            print(l1)


def cp(src_file, target_dir, target_file, hsm='HSM5'):
    HSM = config.HSM_CONFIG.HSM[hsm]
    HSM4 = config.HSM_CONFIG.HSM['HSM4']
    HSM5 = config.HSM_CONFIG.HSM['HSM5']
    login = config.HSM_CONFIG.login
    passwd = config.HSM_CONFIG.password
    print("Do cp %s %s" %(src_file, target_file))

    plink_args = ['plink', '-ssh', HSM5, '-l', login, '-pw', passwd] 
    mkdir_args = ['mkdir', '-p', target_dir, ';']
    copy_args = ['cp', src_file,  target_file, '-v']
    plink_args.extend(mkdir_args)
    plink_args.extend(copy_args)
    proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
    lines = proc.stdout.readlines()
    for l in lines:
        print(l.decode('utf-8').rstrip())

#check path and print if file exist
def test_path_by_list(target_dir, f_list, check_exist):
    count = 1
    for f in f_list:
        f = f.strip()
        if (not f):
            continue
        fpath = os.path.join(target_dir, f)
        if (os.path.isfile(fpath)):
            #print("%i : %s" %(count, fpath))
            count += 1
        else:
            if (not check_exist):
                print("File not found %s" %fpath)

def get_file_no(fpath):
    file_no = ''
    #basename = os.path.basename(fpath)
    #res = basename.splitext(basename)
    #file_no = res[0]

    #match following
    #\\\\mamstream\\MAMDFS\\ptstv\\HVideo\0000\\0000060\\G000006018910002.mxf
    #P000006018910002.mxf
    #P000006018910002
    re_obj = re.compile(r'.*([G|P|D]\d{15}).*') # for checking program
    re_videoid = re.compile(r'^[a-zA-Z0-9]{6,8}$')
    re_news = re.compile(r'.*(\d{15}).*') # for news
    res = re_obj.match(fpath)
    res2 = re_news.match(fpath)
    res3 = re_videoid.match(fpath)
    if (res != None):
        file_no = res.group(1)
    elif (res2 != None):
        file_no = res2.group(1)
    elif (res3 != None):
        file_no = res3.group(2)
    else:
        print('No file number match %s' %fpath)

    return file_no

def mampath2tsmpath(mam_path):
    #print("mam_path = %s" %mam_path)
    tsm_path = ''
    if ('mamstream' in  mam_path):
        #print("convert path")
        item = []
        mam_path = mam_path.rstrip()
        item = mam_path.split('\\')
        #print(item)
        tsm_path = ''
        for i in range(4, len(item), 1):
            #print item[i]
            tsm_path += '/' + item[i]
    else:
        tsm_path = mam_path

    #print("tsm_path = %s" %tsm_path)
    return tsm_path


def my_regex(input, pattern):
    reObj = re.compile(pattern)
    res = reObj.match(input)
    return res


def exec_tsql(conn, tsql):
    c = conn.cursor()
    c.execute(tsql)
    conn.commit()

def exec_select(conn, tsql):
    c = conn.cursor()
    c.execute(tsql)
    row = c.fetchone()
    print(row)
    row = c.fetchone()
    print(row)
    row = c.fetchone()
    print(row)

    '''
    with c.execute(tsql):
        row = c.fetchone()
        while row:
            print(row)
            row = c.fetchone()

    '''

# file_no :
# Gxxx, Pxxx, Dxxx, Video_ID, News_ID
# full path : /ptstv/HVideo/2019/.../G2019...
# /news/pts/2019/..../news_id
# \\mamstream\...
def get_file_path(file_no):
    server = config.MAM_CONFIG.db_server
    database = config.MAM_CONFIG.database
    username = config.MAM_CONFIG.db_user1[0]
    password = config.MAM_CONFIG.db_user1[1]
    connStr = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
    tsql = '' 
    #full path is supported, except video_id
    re_videoid = re.compile(r'.*([a-zA-Z0-9]{6,8}$)')
    re_file_no = re.compile(r'.*([G|P|D]\d{15}$)')
    re_news_file_no = re.compile(r'.*(\d{15}$)')
    Q_PROG = True
    id = ''

    #remove file extension
    arr = os.path.splitext(file_no)
    file_no = os.path.basename(arr[0])
    l = len(file_no)


    res1 = re_file_no.match(file_no)
    if (l == 16 and res1):
        id = res1.group(1)
        l = len(id)
        tsql = ("select FSFILE_PATH_H from TBLOG_VIDEO where FSFILE_NO = ? and FCFILE_STATUS in ('T', 'Y')")

    res2 = re_videoid.match(file_no)
    if (l <= 8 and res2):
        id = res2.group(1)
        l = len(id)
        if (l == 6 or l == 8):
            id = id.zfill(8)
            tsql = ("select FSFILE_PATH_H from TBLOG_VIDEO where FSVIDEO_PROG = ? and FCFILE_STATUS in ('T', 'Y')")

    # news
    res3 = re_news_file_no.match(file_no)
    if (l == 15 and res3):
        id = res3.group(1)
        Q_PROG = False

    # not a valid file number
    if (not (res1 or res2 or res3)):
        return ''

    file_path = ''
    if (Q_PROG):
        cnxn = pyodbc.connect(connStr)
        cursor = cnxn.cursor()

        with cursor.execute(tsql, id):
            row = cursor.fetchone()
            while row:
                file_path = str(row[0].rstrip())
                row = cursor.fetchone()
    else:
        file_path = get_news_file_path(file_no)

    return file_path


def get_file_path_by_prog_id(prog_id, episodes = None):
    server = config.MAM_CONFIG.db_server
    database = config.MAM_CONFIG.database
    username = config.MAM_CONFIG.db_user1[0]
    password = config.MAM_CONFIG.db_user1[1]
    connStr = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
    tsql = "select FSFILE_NO, FSFILE_PATH_H, FSVIDEO_PROG from TBLOG_VIDEO where FSID = ? and FNEPISODE in ("  + episodes  + ") and FSARC_TYPE in ('001', '002') and FCFILE_STATUS = 'T'"

    cnxn = pyodbc.connect(connStr)
    cursor = cnxn.cursor()

    file_path_list = []
    with cursor.execute(tsql, prog_id):
        row = cursor.fetchone()
        while row:
            file_path = str(row[1].rstrip())
            file_path_list.append(file_path)
            row = cursor.fetchone()
    
    cnxn.close()

    return file_path_list

def get_prog_name():
    TARGET_DIR = '\\\\10.13.200.6\MAMdownload\GPFS\TEST\\'
    server = config.MAM_CONFIG.db_server
    database = config.MAM_CONFIG.database
    username = config.MAM_CONFIG.db_user1[0]
    password = config.MAM_CONFIG.db_user1[1]
    connStr = 'DRIVER={{ODBC Driver 13 for SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
    
    cnxn = pyodbc.connect(connStr)
    cursor = cnxn.cursor()
    tsql = ("select PROG_ID, FSVIDEO_PROG, FSFILE_NO, NAME from VW_TBLOG_VIDEO_PROG_ALL order by PROG_ID, FSFILE_NO")
    fh = open(TARGET_DIR + "prog_name.txt", "w", encoding="utf-8")

    count = 0
    with cursor.execute(tsql):
        row = cursor.fetchone()
        while row:
            prog_id = str(row[0].rstrip())
            video_id = str(row[1].rstrip())
            file_no = str(row[2].rstrip())
            name = str(row[3].rstrip())
            newname = re.sub('[\\/:\*\?"\<\>\|]', '-', name)
            fname = prog_id + "_" + video_id + "_" + file_no + "_" + newname
            file_path = TARGET_DIR + fname
            #print(str(count) + ":" + file_path)
            if (name != newname):
                fh.writelines(fname + "\n")
                res = create_gpfs_file(file_path)
                
            row = cursor.fetchone()
            count = count + 1



#To get programe and video_id and file_no information
def get_prog_file_info(type, build_file_struct):
    TARGET_DIR = '\\\\10.13.200.30\\GPFS\\'
    server = config.MAM_CONFIG.db_server
    database = config.MAM_CONFIG.database
    username = config.MAM_CONFIG.db_user1[0]
    password = config.MAM_CONFIG.db_user1[1]
    connStr = 'DRIVER={{ODBC Driver 13 for SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)

    date_time = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime())
    current_date = time.strftime('%Y-%m-%d', time.localtime())
    cnxn = pyodbc.connect(connStr)
    cursor = cnxn.cursor()

    if (type == '0'): #dump all
        print("dump all video info")
        tsql = ("select PROG_ID, FSVIDEO_PROG, FSFILE_NO, FCFILE_STATUS, NAME, FSFILE_PATH_H from VW_TBLOG_VIDEO_PROG_ALL order by PROG_ID, FSFILE_NO")
        f_output = TARGET_DIR + "tblog_video_all_" + date_time + ".txt"
    elif (type == '1'): #dump new updated today
        print("dump new updated video info")
        tsql = ("select PROG_ID, FSVIDEO_PROG, FSFILE_NO, FCFILE_STATUS, NAME, FSFILE_PATH_H from VW_TBLOG_VIDEO_PROG_ALL where FDUPDATED_DATE >= '" + current_date +  "' order by PROG_ID, FSFILE_NO")
        f_output = TARGET_DIR + "tblog_video_updated_" + date_time + ".txt"
    elif (type == '2'): # new created today
        print("dump new created video info")
        tsql = ("select PROG_ID, FSVIDEO_PROG, FSFILE_NO, FCFILE_STATUS, NAME, FSFILE_PATH_H from VW_TBLOG_VIDEO_PROG_ALL where FDCREATED_DATE >= '" + current_date +  "' order by PROG_ID, FSFILE_NO")
        f_output = TARGET_DIR + "tblog_video_created_" + date_time + ".txt"
    else:
        print("Wrong type")
        return 1

    fh = open(f_output, "w", encoding='utf-8')
    count = 1
    with cursor.execute(tsql):
        row = cursor.fetchone()
        while row:
            record = ""
            prog_id = str(row[0].rstrip())
            video_id = str(row[1].rstrip())
            file_no = str(row[2].rstrip())
            file_status = str(row[3].rstrip())
            name = str(row[4].rstrip())
            #replace special character with '-'
            name = re.sub('[\\/:\*\?"\<\>\|\t]', '-', name)
            file_path = str(row[5].rstrip())
            str_arr = file_path.split('\\')
            prog_dir = prog_id + "-" + name
            file_name = file_no + "_" + video_id
            gpfs_path_list_l = []
            gpfs_path_list_w = []
            for item in str_arr[4:]:
                gpfs_path_list_l.append('/')
                gpfs_path_list_l.append(item)
                gpfs_path_list_w.append('\\')
                gpfs_path_list_w.append(item)

            gpfs_path_l = ''.join(gpfs_path_list_l)
            gpfs_path_w = ''.join(gpfs_path_list_w)
            channel = str_arr[4]
            year = str_arr[6]
            new_gpfs_path_l = "/{c}/HVideo/{y}/{p}/{f}.mxf".format(c=channel, y=year, p=prog_dir, f=file_name)
            new_gpfs_path_w = "\\{c}\\HVideo\\{y}\\{p}\\{f}.mxf".format(c=channel, y=year, p=prog_dir, f=file_name)
            out = prog_dir + ":" + file_name + ":" + file_status + ":" + file_path + ":" + gpfs_path_l + ":" + new_gpfs_path_l + ":" + new_gpfs_path_w + "\n" 
            #print(str(count) + ":" + out)
            fh.writelines(out)
            #if (os.path.isdirs())

            if (build_file_struct == 1): #create file
                gpfs_path = TARGET_DIR + new_gpfs_path_w
                res = create_gpfs_file(gpfs_path)

            row = cursor.fetchone()


def create_gpfs_file(gpfs_path):
    folder = os.path.dirname(gpfs_path)
    if (not os.path.exists(folder)):
        os.makedirs(folder)

    stud_path = gpfs_path + ".stud"
    if (not os.path.exists(gpfs_path) and not os.path.exists(stud_path)):
        print("create " + stud_path)
        try:
            fh = open(stud_path, "w")
            return True
        except Exception as e:
            print(str(e)) 
            return False

    
#run in windows
#get all plylist information
def get_playlist_all(pdate, channel):
    server = '10.13.210.33'
    nas1 = '10.13.200.22'
    HDUPLOAD_DIR = '\\\\10.13.200.25\\HDUpload'
    REVIEW_DIR = '\\\\10.13.200.22\\mamnas1\\Review'
    APPROVED_DIR = '\\\\10.13.200.22\\mamnas1\\Approved'
    #server = '10.13.220.34'
    database = 'MAM'
    username = 'audiotest' #db owner for executing SPL procedure
    password = 'audiotest'

    connStr = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)

    fvideo_id = "videoid-" + pdate + "-" + channel + ".txt"
    forgid = "org-" + pdate + "-" + channel + ".txt"
    fnewid = "new-" + pdate + "-" + channel + ".txt"

    playlist = []
    video_id_dict = {}
    count = 0 #count queue for all str_channel
    count_ch = 0 #count queue number for specified channel
    cnxn = pyodbc.connect(connStr)
    cursor = cnxn.cursor()
    #tsql = ("exec SP_Q_GET_PLAY_VIDEOID_LIST")
    tsql = ("select * from VW_PLAYLIST_FROM_NOW order by FDDATE, FSCHANNEL_ID, QUEUE")

    fh = open(fvideo_id, "w", encoding= "utf-8")
    fo = open(forgid, "w", encoding = "utf-8")
    fn = open(fnewid, "w", encoding = "utf-8")

    with cursor.execute(tsql):
        row = cursor.fetchone()
        while row:
            record = ""
            queueno = int(row[0])
            name = str(row[1]).rstrip()
            episode = int(row[2])
            fddate = str(row[3])
            fsplaytime = str(row[4])
            fschannel = str(row[5])
            video_id = str(row[6]).rstrip()
            video_id = video_id[2:]
            queue = [queueno, name, episode, fddate, fsplaytime, fschannel, video_id]
            playlist.append(queue)
            if (fddate == pdate and fschannel == channel):
                if (len(video_id) > 0 and video_id != 'None' and video_id not in video_id_dict):  # use regular expression instead
                    video_id_dict[video_id] = queue
                    test_id = 'T' + video_id[1:]
                    lines = video_id + " " + test_id + " " + str(queue) + "\n"
                    fh.writelines(lines)
                    fo.writelines(video_id + "\n")
                    fn.writelines(test_id + "\n")
                    count_ch = count_ch + 1

            """
            for i in range(len(row)):
                record = record + str(row[i]).rstrip() + "|"
            print(record)
            """
            count = count + 1
            row = cursor.fetchone() #end of get_playlist_all

    for q in playlist:
        if (q[3] == pdate and q[5] == channel):
            print(q)
    print("Total number of queue : " + str(count))

    #check if file exist in Approved folder
    not_found = []
    for vid in video_id_dict.keys():
        print(vid + ":" + str(video_id_dict[vid]))
        target_file = APPROVED_DIR + "\\" + vid + ".mxf"
        if (os.path.isfile(target_file)):
            print ("Found " + target_file)
        else: #need check GPFS and volume status use --dsmls
            print ("Cannot find " + target_file)
            not_found.append(vid)

    print("\n\nTotal number of queue (all channel): " + str(count))
    print("Total number of queue (ch-" + channel +  ", " + pdate + "): " + str(count_ch))
    print("Total number of file: " + str(len(video_id_dict)))
    print("Total number of file not found: " + str(len(not_found)))
    print("\n\n--------------------------- File not found ---------------------------------")
    for vid in not_found:
        print(str(video_id_dict[vid]) + " not found")

    #need check GPFS status and volume status
    NEED_COPY = 1
    if (NEED_COPY):
        for vid in not_found:
            v = vid.zfill(8)
            video_file = video_id_dict[vid][6] + '.mxf'
            hdupload_path = os.path.join(HDUPLOAD_DIR, video_file)
            review_path = os.path.join(REVIEW_DIR, video_file)
            if (os.path.isfile(hdupload_path)):
                print("copy %s from HDUpload to Approved" %video_file)
                os.system("copy " + hdupload_path + " " + APPROVED_DIR)
                print("Create Louth DB %s" %v)
                os.system(LOUTH_DB_GENERATE + " " + vid.zfill(8))
                create_louth_db(vid.zfill(8))
            elif (os.path.isfile(review_path)):
                print("copy %s from Review to Approved" %video_file)
                os.system("copy " + review_path + " " + APPROVED_DIR)
                create_louth_db(vid.zfill(8))

    return video_id_dict

# copy file from HDUpload, or from GPFS
def copy2gpfs_by_video_id(video_id):
    HDUPLOAD_DIR = '\\\\10.13.200.25\\HDUpload'
    REVIEW_DIR = '\\\\10.13.200.22\\mamnas1\\Review'
    APPROVED_DIR = '\\\\10.13.200.22\\mamnas1\\Approved'
    video_file = video_id + ".mxf"
    hdupload_path = os.path.join(HDUPLOAD_DIR, video_file)
    review_path = os.path.join(REVIEW_DIR, video_file)
    approved_path = os.path.join(APPROVED_DIR, video_file)

    if (os.path.isfile(hdupload_path)):
        print("copy %s from HDUpload to Approved" %video_file)
        os.system("copy " + hdupload_path + " " + APPROVED_DIR)
        create_louth_db(video_id.zfill(8))
    elif (os.path.isfile(review_path)):
        print("copy %s from Review to Approved" %video_file)
        os.system("copy " + review_path + " " + APPROVED_DIR)
        create_louth_db(video_id.zfill(8))
    else: #check GPFS, and copy
        pass


def create_louth_db(video_id):
    cmd = "{l} {v}".format(l=LOUTH_DB_GENERATE, v=video_id)
    print("Create Louth DB %s" %video_id)
    os.system(cmd)

def get_event_by_video_id(vid):
    server = config.MAM_CONFIG.db_server
    database = config.MAM_CONFIG.database 
    username = config.MAM_CONFIG.db_user2[0]
    password = config.MAM_CONFIG.db_user2[1]
    vid = vid.zfill(8)

    connStr = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)

    playlist = []
    video_id_dict = {}
    count = 0 #count queue for all str_channel
    count_ch = 0 #count queue number for specified channel
    cnxn = pyodbc.connect(connStr)
    cursor = cnxn.cursor()
    #tsql = ("exec SP_Q_GET_PLAY_VIDEOID_LIST")
    tsql = ("select QUEUE, NAME, FNEPISODE, FDDATE, FSPLAY_TIME, FSCHANNEL_ID, FSVIDEO_ID from VW_PLAYLIST_FROM_NOW where FSVIDEO_ID = ? order by FDDATE, FSPLAY_TIME")

    with cursor.execute(tsql, vid):
        row = cursor.fetchone()
        while row:
            record = ""
            queueno = int(row[0])
            name = str(row[1]).rstrip()
            episode = int(row[2])
            fddate = str(row[3])
            fsplaytime = str(row[4])
            fschannel = str(row[5])
            video_id = str(row[6]).rstrip()
            video_id = video_id[2:]
            queue = [queueno, name, episode, fddate, fsplaytime, fschannel, video_id]
            playlist.append(queue)

            row = cursor.fetchone()

    return playlist 

# To move files by video id list
def move_file_by_video_id(video_id_dict, src_dir, target_dir):
    for id in video_id_dict.keys():
        print("Check " + id + "  ------")
        src_f = src_dir + "\\" + id + ".mxf"
        target_f = target_dir + "\\" + id + ".mxf"

        if (os.path.isfile(target_f)):
            print("Found " + target_f)
        else:
            if (os.path.isfile(src_f)):
                print("move file to  " + target_f)
                try:
                    os.rename(src_f, target_f)
                except Exception:
                    print("Permission denied")
                    continue

#copy file by video id list
def copy_file_by_video_id(video_id_dict, src_dir, target_dir):
    count_cp = 0
    count_not_found = 0
    total = len(video_id_dict)

    for id in video_id_dict.keys():
        print("Check " + id + "  ------")
        src_f = src_dir + "\\" + id + ".mxf"
        new_id = 'T' + id[1:]
        target_f = target_dir + "\\" + new_id + ".mxf"
        check_path = src_dir + "\\" + new_id + ".mxf"

        if (os.path.isfile(check_path)):
            print(check_path + " already exist")
            continue

        if (os.path.isfile(target_f)):
            print("Found " + target_f)
        else:
            if (os.path.isfile(src_f)):
                count_cp = count_cp + 1
                print("Copy file to {} ({}, {})".format(target_f, count_cp, total))
                try:
                    #shutil.copyfie(src_f, target_f)
                    shutil.copy(src_f, target_f)
                except Exception:
                    print("Permission denied")
                    continue

# To move files by video id list, 002K3B -> T02K3B
def rename_video_id(video_id_dict, src_dir, target_dir):
    for id in video_id_dict.keys():
        print("Check " + id + "  ------")
        new_id  = 'T' + id[1:]
        src_f = src_dir + "\\" + id + ".mxf"
        target_f = target_dir + "\\" + new_id + ".mxf"

        if (os.path.isfile(target_f)):
            print("Found " + target_f)
        else:
            if (os.path.isfile(src_f)):
                print("move file to  " + target_f)
                try:
                    os.rename(src_f, target_f)
                except Exception:
                    print("Permission denied")
                    continue

# rename T03EA8.mxf -> 003EA8.mxf
def rename_test_id(target_dir):
    extObj = re.compile(r'T0.*\.mxf$', re.IGNORECASE)
    count = 0
    for r, ds, fs in os.walk(target_dir):
        #print(fs)
        for f in fs:
            src_path = os.path.join(r, f)
            #print(src_path)
            result = extObj.match(f)
            if (result != None):
                newf = '0' + f[1:]
                new_path = os.path.join(r, newf)
                if (not os.path.isfile(new_path)):
                    os.rename(src_path, new_path)
                #print(src_path + "-->" + new_path)
                    count = count + 1
                #print(result.group(0))
    print("rename " + str(count) + " files")

def modify_louth_lst(lst):
    out = "new_id.lst"
    CHUNKSIZE = 104

    fh = open(lst, "rb")
    fo = open(out, "wb")

    bytes_read = fh.read(CHUNKSIZE)
    while bytes_read:
        video_id = bytes_read[16:24]
        str_video_id = video_id.decode("utf-8")
        if (str_video_id[0:2] == '00'):
            str_video_id = 'T' + str_video_id[1:]
            video_id = str_video_id.encode("utf-8")
            #print(len(video_id)) #8
            newbyte_array = bytes_read[0:16] + video_id + bytes_read[24:104]
            fo.write(newbyte_array)
        else:
            fo.write(bytes_read)

        bytes_read = fh.read(CHUNKSIZE)

# query libvol TS3500/TS3310, only volumn name
def get_libvol(lib_name):
    vol_list = []
    count = 0

    HSM = config.HSM_CONFIG.HSM4
    login = config.HSM_CONFIG.login
    passwd = config.HSM_CONFIG.password
    tsm_login = config.TSM_CONFIG.tsm_user1[0]
    tsm_passwd = config.TSM_CONFIG.tsm_user1[1]

    plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
    dsmadmc_args = ['dsmadmc', '-id={p}'.format(p=tsm_login), '-password={p}'.format(p=tsm_passwd), 'query', 'libvol', lib_name]
    plink_args.extend(dsmadmc_args)
    proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
    lines = proc.stdout.readlines()
    reObj = re.compile(r'[A-Z]{1}\d{5}L4')
    for l in lines:
        l = str(l)
        tmplist = re.split(r'\s+', l)
        if (len(tmplist) > 1):
            res = reObj.match(tmplist[1])
            if (res):
                vol = res.group(0)
                vol_list.append(vol)
                count += 1

    print("Total vol in {0}: {1}".format(lib_name, str(len(vol_list))))
    return vol_list

# query libvol TS3500/TS3310
def get_libvol_info(lib_name):
    vol_list = []
    count = 0

    HSM = config.HSM_CONFIG.HSM4
    login = config.HSM_CONFIG.login
    passwd = config.HSM_CONFIG.password
    tsm_login = config.TSM_CONFIG.tsm_user1[0]
    tsm_passwd = config.TSM_CONFIG.tsm_user1[1]

    plink_args = "plink -ssh {hsm} -l {login} -pw {passwd}".format(hsm = HSM, login = login, passwd = passwd)
    #plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
    dsmadmc_args = "dsmadmc -id={l} -password={p} query libvol {lib}".format(l = tsm_login, p = tsm_passwd, lib = lib_name)
    #dsmadmc_args = ['dsmadmc', '-id={p}'.format(p=tsm_login), '-password={p}'.format(p=tsm_passwd), 'query', 'libvol', lib_name]
    #plink_args.extend(dsmadmc_args)
    cmd = "{0} {1}".format(plink_args, dsmadmc_args)
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    lines = proc.stdout.readlines()
    reObj = re.compile(r'[A-Z]{1}\d{5}L4')
    for l in lines:
        l = str(l)
        print(l)
        continue
        tmplist = re.split(r'\s+', l)
        if (len(tmplist) > 1):
            res = reObj.match(tmplist[1])
            if (res):
                vol = res.group(0)
                vol_list.append(vol)
                count += 1

    print("Total vol in {0}: {1}".format(lib_name, str(len(vol_list))))
    return vol_list

def get_unavail_vol():
    vol_list = []

    HSM = config.HSM_CONFIG.HSM4
    login = config.HSM_CONFIG.login
    passwd = config.HSM_CONFIG.password
    tsm_login = config.TSM_CONFIG.tsm_user1[0]
    tsm_passwd = config.TSM_CONFIG.tsm_user1[1]

    plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
    dsmadmc_args = ['dsmadmc', '-id={p}'.format(p = tsm_login), '-password={p}'.format(p = tsm_passwd), 'query', 'vol', 'access=unavailable']
    plink_args.extend(dsmadmc_args)
    #print(plink_args)
    proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
    lines = proc.stdout.readlines()
    reObj = re.compile(r'[A-Z]{1}\d{5}L4')
    for l in lines:
        l = l.decode('utf-8')
        #l = str(l)
        #print(l)
        tmplist = re.split(r'\s+', l)
        if (len(l) > 1):
            res = reObj.match(tmplist[0])
            if (res):
                vol = res.group(0)
                vol_list.append(vol)

    print("Total unavailable vol : {0}".format(str(len(vol_list))))
    
    return vol_list


def get_file_size(path):
    size = 0
    if (not os.path.isfile(path)):
        size = -1 
    else:
        statInfo = os.stat(path)
        size = statInfo.st_size

    return size

# file_no_list = ['news_file_no', 'promo_no', 'Gxxxxx..']
# vol_dir = \\10.13.200.30\GPFS\VOL\
def get_tape_info_by_fileno(file_no_list, vol_dir):
    tape_info = {}
    for r, ds, fs in os.walk(vol_dir):
        for f in fs:
            if ("_" in f):
                continue
            
            file_name = vol_dir + "\\" + f
            fh = open(file_name, "r")
            lines = fh.readlines()
            for l in lines:
                for file_no in file_no_list:
                    if (file_no in l):
                        tape_info[file_no] = f
            
            fh.close()

    return tape_info

#extract vol name from file NEWSHAKKAHSM.txt
#query vol STGpool=NEWSHAKKAHSM
#A00010L4                     NEWSHAKKAH-     NEWSHAKKA-       916.1 G     100.0       Full
def get_vol_name(infile, vol_type):
    fh = open(infile, "r")
    lines = fh.readlines()
    count = 1
    vol_list = []

    for l in lines:
        l = l.rstrip()
        list = re.split("\s+", l)
        #print(list)
        reObj = re.compile(r'[A-Z]{1}\d{5}L4')
        #reObj1 = re.compile(r'([A-Z]{1}\d{5}L4).*(Full|Filling)')
        reObj1 = re.compile(r'Full|Filling')
        res = reObj.match(list[0])
        if (res):
            vol_name = res.group(0)
            if (len(list) == 7):
                if (vol_type == 1): #get full volume
                    if (list[6] == 'Full'): #only dump full volume
                        vol_list.append(vol_name)
                        count = count + 1
                elif (vol_type == 2):
                    if (list[6] == 'Filling'): #only dump filling volume
                        vol_list.append(vol_name)
                        count = count + 1
                else: #all dump, Full  + Filling
                    vol_list.append(vol_name)
                    count = count + 1

    return vol_list

def dump_vol_content_info(vol, outf):
    query_args = 'query content {v} > {o}'.format(v = vol, o = outf)
    cmd  = "{conn} {query}".format(conn = connection, query = query_args)
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
    lines = proc.stdout.readlines()
    exit_code = proc.wait()
    for l in lines:
        l = l.decode('utf-8').strip()
        print(l)

def dump_stgp_vol_info(stgp, outf):        
    query_args = "query vol STGPool={pool} > {outf}".format(pool = stgp, outf = outf)
    cmd  = "{conn} {query}".format(conn = connection, query = query_args)
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
    lines = proc.stdout.readlines()
    exit_code = proc.wait()
    for l in lines:
        l = l.decode('utf-8').strip()
        print(l)

#default dump Full, 16
def dump_libvol_info(libname, stgp = '', vol_type = 0, max = 16):
    vol_list = get_libvol(libname)
    count = 0
    for v in vol_list:
        #query_args = "query vol {volname} | sed 1,13d".format(volname = v)
        query_args = "query vol {volname}".format(volname = v)
        #linux_args = "| sed 1,13d"
        #cmd = [connection, query_args, linux_args]
        cmd = "{conn} {query}".format(conn = connection, query = query_args)
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        lines = proc.stdout.readlines()
        exit_code = proc.wait()
        for l in lines:
            l = l.decode('utf-8').strip()
            l = l.rstrip()
            list = re.split("\s+", l)
            reObj = re.compile(r'[A-Z]{1}\d{5}L4')
            #reObj1 = re.compile(r'([A-Z]{1}\d{5}L4).*(Full|Filling)')
            reObj1 = re.compile(r'Full|Filling')
            res = reObj.match(list[0])
            if (res):
                if (len(list) == 7):
                    vol_name = list[0]
                    pool = list[1]
                    size = list[3]
                    unit = list[4] 
                    pct = list[5]
                    vol_type = list[6]

                    if (pool == stgp):
                        count += 1
                        print("%i %s" %(count, list))

        if (count == max):
            break


def test_case(tcase):
    if (tcase == 1):
        print("test case 1")

def main(argv):
    print("main")                        
    try:
        opts, args = getopt.getopt(argv, "t:", ["tcase="] )
    except Exception as e:
        template = "Exception {0} occur : {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)
        sys.exit()

    for opt, arg in opts:
        if (opt in ("-t", "--tcase")):
            tcase = int(arg)
            test_case(tcase)

if (__name__ == "__main__"):
    main(sys.argv[1:])                

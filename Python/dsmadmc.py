# -*- coding: UTF-8 -*-

import config
import getopt
import os
import subprocess
import sys

DO_Q_LIBVOL = False
DO_Q_STATUS = False
DO_Q_SCRATCH = False
DO_Q_TAPE = False
DO_UPDATE_UNAVAIL = False
BY_STGPOOL = False
BY_VOL = False


hsm_login = config.HSM_CONFIG.login
hsm_passwd =  config.HSM_CONFIG.password
tsm_login = config.TSM_CONFIG.tsm_user1[0] 
tsm_passwd = config.TSM_CONFIG.tsm_user1[1]
hsm = config.HSM_CONFIG.HSM['HSM4']
LOUTH_DB_GENERATE = '.\LOUTH-DB-Create.exe'

plink_args = 'plink -ssh {hsm} -l {login} -pw {passwd}'.format(hsm = hsm, login = hsm_login, passwd = hsm_passwd)
dsmadmc_args = 'dsmadmc -id={login} -password={passwd}'.format(login = tsm_login, passwd = tsm_passwd)
connection = '{plink} {dsmadmc}'.format(plink = plink_args, dsmadmc = dsmadmc_args)

def exec_subprocess(cmd):
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    #exit_code = proc.wait()
    #print(exit_code)
    lines = proc.stdout.readlines()
    for l in lines:
        l = l.decode('utf-8').strip()
        print(l)


def query_libvol(libvol):
    query = "query libvol {0}".format(libvol)
    cmd = "{0} {1}".format(connection, query)
    exec_subprocess(cmd)


def query_status():
    query = "query status"
    cmd = "{0} {1}".format(connection, query)
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    exit_code = proc.wait()
    print(exit_code)
    lines = proc.stdout.readlines()
    for l in lines:
        l = l.decode('utf-8').strip()
        print(l)

def query_scratch(lib_name):
    two_pass = False

    if (1): #work, good for this
        qcmd = "{0} query libvol {1} | grep Scratch".format(dsmadmc_args, lib_name)
        cmd = plink_args.split(' ') + [qcmd]

    if (0): #work
        plink = ['plink', '-ssh', hsm, '-l', hsm_login, '-pw', hsm_passwd] 
        grep_args = [dsmadmc_args, 'q libvol {0}'.format(lib_name),  '| grep Scratch']
        #grep_args = ['q libvol {0}'.format(lib_name),  '| grep Scratch']
        cmd = plink + grep_args

    if (0): #work
        grep_args = ['q libvol {0}'.format(lib_name),  '|grep Scratch']
        #grep_args = ['q libvol {0}'.format(lib_name),  '|tee']  #no output
        cmd = connection.split(' ') + grep_args
    
    if (0): #not work
        qcmd = "query libvol {0} | grep Scratch".format(lib_name)
        cmd_str = "{0} {1}".format(connection, qcmd)
        cmd = cmd_str.split(' ')

    if (0): # not work
        qcmd = "query libvol {0}".format(lib_name)
        grep = '| grep Scratch'
        cmd = [connection, qcmd, grep]

    if (0): # not work
        qcmd = "{0} query libvol {1}".format(dsmadmc_args, lib_name)
        grep = '| grep Scratch'
        cmd = [plink_args, qcmd, grep]

    if (0): #work
        qcmd = "query libvol {0}".format(lib_name)
        grep = '| grep Scratch'
        cmd = plink_args.split(' ') + [dsmadmc_args, qcmd, grep]

    outf = "libvol.txt"
    if (0): #2 pass, work
        two_pass = True
        qcmd = "query libvol {0} > {1}".format(lib_name, outf)
        cmd = "{0} {1}".format(connection, qcmd)


    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    exit_code = proc.wait()
    print(exit_code)
    print(cmd)
    lines = proc.stdout.readlines()
    for l in lines:
        l = l.decode('utf-8').strip()
        print(l)

    if (two_pass):
        fh = open(outf, "r", encoding='utf-8')
        lines = fh.readlines()
        for l in lines:
            if ("Scratch" in l):
                print(l.strip())


def query_tape(file_no):
    plink = ['plink', '-ssh', hsm, '-l', hsm_login, '-pw', hsm_passwd] 
    grep_args = ['cd /mnt/MAM_ARCHIVE/TSM_INFO/VOL/HSM;', 'grep', file_no, '*']
    plink.extend(grep_args)

    qcmd = "cd /mnt/MAM_ARCHIVE/TSM_INFO/VOL/HSM; grep {0} *".format(file_no)
    cmd = "{0} {1}".format(plink_args, qcmd)
    
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    exit_code = proc.wait()
    print(exit_code)
    lines = proc.stdout.readlines()
    for l in lines:
        l = l.decode('utf-8').strip()
        print(l)

# update unavailable vol = all | vol_name
def update_unavail_vol(vol, stgp):
    global BY_STGPOOL
    if (BY_STGPOOL):
        update = "update volume \* access=READWRITE whereaccess=UNAVAIL wherestgpool={0}".format(stgp)
    else:
        if (vol == "all"):
            update = "update volume \* access=READWRITE whereaccess=UNAVAIL"
        else: #update all
            update = "update volume {0} access=READWRITE".format(vol)

        cmd = "{0} {1}".format(connection, update)
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        exit_code = proc.wait()
        print(exit_code)
        lines = proc.stdout.readlines()
        for l in lines:
            l = l.decode('utf-8').strip()
            print(l)

def usage():
    usage_str = """
NAME
    dsmadmc - TSM admin

SYNOPSIS
    dsmadmc [OPTION]

DESCRIPTION

    --dsmmigrate

    --q_libvol=lib_name

    --q_status

    --q_scratch

    --by_stgp=STGPOOL

    --by_vol=VOLUME_NAME

    --u_unavail=VOLUME_NAME|all
    """

    print(usage_str)

def main(argv):
    global DO_Q_LIBVOL
    global DO_Q_STATUS
    global DO_Q_SCRATCH
    global DO_Q_TAPE
    global DO_UPDATE_UNAVAIL
    global BY_STGPOOL
    #global gv
    stgp = None
    vol = None
    lib_name = None
    file_no = None
    
    try:
        opts, args = getopt.getopt(argv, "h", 
        [
            "dsmmigrate"
            , "q_libvol="
            , "q_status"
            , "q_scratch="
            , "q_tape="
            , "by_stgp=" #specify storage pool
            , "by_vol=" #specify volume
            , "u_unavail=" # specify volumn name for updating. all | vol_name 
        ])
    except Exception as e:
        template = "Exception {0} occur : {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)

        sys.exit(2)
    
    for opt, arg in opts:
        print("Process opt %s, %s" %(opt, arg))
        if (opt in ("-h")):
            usage()
            sys.exit()
        elif (opt == "--q_libvol"):
            DO_Q_LIBVOL = True
            libvol = arg
        elif (opt == "--q_status"):
            DO_Q_STATUS = True

        elif (opt == "--q_scratch"):
            DO_Q_SCRATCH = True
            lib_name = arg
        elif (opt == "--q_tape"):
            DO_Q_TAPE = True
            file_no = arg
        elif (opt == "--by_stgp"): #specify stgpool
            BY_STGPOOL = True
            stgp = arg
        elif (opt == "--u_unavail"):
            DO_UPDATE_UNAVAIL = True
            vol = arg  # all | vol_name

    if (DO_Q_LIBVOL):
        query_libvol(libvol)

    if (DO_Q_STATUS):
        query_status()

    if (DO_Q_SCRATCH):
        query_scratch(lib_name)

    if (DO_Q_TAPE):
        query_tape(file_no)

    if (DO_UPDATE_UNAVAIL):
        update_unavail_vol(vol, stgp)


if (__name__ == "__main__"):
    main(sys.argv[1:])
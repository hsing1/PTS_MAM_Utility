# -*- coding: UTF-8 -*-

import os
import sys
import pyodbc
import config
import getopt
import time
import re
import subprocess
import ptsutils3
from datetime import datetime

version =  '20190114'
server = config.NEWNEWS_CONFIG.db_server
database = config.NEWNEWS_CONFIG.database 
username = config.NEWNEWS_CONFIG.db_user[0]
password = config.NEWNEWS_CONFIG.db_user[1]
conn_str = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)

def query_upload_temp_by_status(begin_time = None, end_time = None, file_status = 1, converter_status = 1):
    print("DO query_upload_temp_by_status")
    cnxn = pyodbc.connect(conn_str)
    cursor = cnxn.cursor()
    tsql_by_status = ''

    if (begin_time and end_time):
        tsql_by_status = ("select UploadID, VideoName, VideoSize, VideoTime, \
        ArchiveName, MediaInfo, FileStatus, ConverterStatus, \
        CreateDate, ConfirmMark, Location, ConfirmUser \
        from UploadTemp where CreateDate between ? and ? and FileStatus = ? and ConverterStatus = ? and  ConfirmMark = 1 order by CreateDate")
        upload_info = []
        with cursor.execute(tsql_by_status, begin_time, end_time, file_status, converter_status):
            row = cursor.fetchone()
            while row:
                row_info = []
                upload_id = str(row[0])
                video_name = str(row[1])
                archive_name = str(row[4])
                file_status = str(row[6])
                converter_status = str(row[7])
                create_date = str(row[8])
                confirm_mark = str(row[9])
                location = str(row[10])
                confirm_user = str(row[11])

                #print("%s, %s, %s, %s, %s, %s" %(upload_id, video_name, archive_name, file_status, converter_status, create_date))
                row_info.extend([upload_id, video_name, archive_name, file_status, converter_status, 
                    create_date, confirm_mark, location, confirm_user])
                upload_info.append(row_info)
                row = cursor.fetchone()
    else:
        tsql_by_status = ("select UploadID, VideoName, VideoSize, VideoTime, \
        ArchiveName, MediaInfo, FileStatus, ConverterStatus, \
        CreateDate, ConfirmMark, Location, ConfirmUser \
        from UploadTemp where FileStatus = ? and ConverterStatus = ? and  ConfirmMark = 1 order by CreateDate")

        upload_info = []
        with cursor.execute(tsql_by_status, file_status, converter_status):
            row = cursor.fetchone()
            while row:
                row_info = []
                upload_id = str(row[0])
                video_name = str(row[1])
                archive_name = str(row[4])
                file_status = str(row[6])
                converter_status = str(row[7])
                create_date = str(row[8])
                confirm_mark = str(row[9])
                location = str(row[10])
                confirm_user = str(row[11])

                #print("%s, %s, %s, %s, %s, %s" %(upload_id, video_name, archive_name, file_status, converter_status, create_date))
                row_info.extend([upload_id, video_name, archive_name, file_status, converter_status, 
                    create_date, confirm_mark, location, confirm_user])
                upload_info.append(row_info)
                row = cursor.fetchone()
    
    #print(upload_info)
    return upload_info


def query_upload_temp_by_upload_id(upload_id):
    print("DO query_info_by_upload_id:" + upload_id)
    cnxn = pyodbc.connect(conn_str)
    cursor = cnxn.cursor()
    tsql_by_upload_id = ("select UploadID, VideoName, VideoSize, VideoTime, \
    ArchiveName, MediaInfo, FileStatus, ConverterStatus, \
    CreateDate, ConfirmMark, Location, ConfirmUser \
    from UploadTemp where UploadID = ?")

    with cursor.execute(tsql_by_upload_id, upload_id):
        row = cursor.fetchone()
        while row:
            row_info = []
            upload_id = str(row[0])
            video_name = str(row[1])
            archive_name = str(row[4])
            file_status = str(row[6])
            converter_status = str(row[7])
            create_date = str(row[8])
            confirm_mark = str(row[9])
            location = str(row[10])
            confirm_user = str(row[11])

            #print("%s, %s, %s, %s, %s, %s" %(upload_id, video_name, archive_name, file_status, converter_status, create_date))
            row_info.extend([upload_id, video_name, archive_name, file_status, converter_status, 
                create_date, confirm_mark, location, confirm_user])
            row = cursor.fetchone()
    cnxn.close()
    print(row_info)
    return row_info


def move_to_converter(begin_time = None, end_time = None):
    upload_info = query_upload_temp_by_status(begin_time, end_time)
    count = 1
    for row in upload_info:
        upload_id = row[0]
        print(str(count) + ":Process " + upload_id)
        move_to_converter_by_id(upload_id)
        count += 1

def move_to_converter_by_id(upload_id):
    print("DO_move_to_converter " + upload_id)
    source_dir = "\\\\10.13.200.15\\PreConvert"
    target_dir = "\\\\10.13.200.56\\source_mxf"
    upload_info = query_upload_temp_by_upload_id(upload_id)
    archive_name = upload_info[2]
    src_file = os.path.join(source_dir, archive_name)

    if (os.path.isfile(src_file)):
        cmd = "copy " + src_file + " " + target_dir
        try:
            print(cmd)
            os.system(cmd)
        except Exception as e:
            template = "Exception {0} occur : {1!r}"
            message = template.format(type(e).__name__, e.args)
            print(message)

def newscg(source):
    NEWSCG = "10.1.163.1"
    login = "newscg"
    passwd = "p@ssw0rd"
    plink_args = "plink -ssh {cg} -l {l} -pw {p}".format(cg = NEWSCG, l = login, p = passwd)
    cgcommand = "/exc/doc -g `cat /list/{src}` > {out}.txt".format(src = source, out = source)
    cmd = "{plink} {cg}".format(plink = plink_args, cg = cgcommand)

    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    lines = proc.stdout.readlines()
    for l in lines:
        print(l.decode('utf-8').rstrip())

        

def main(argv):
    DO_MOVE_TO_CONVERTER = False
    DO_MOVE_TO_CONVERTER_BY_ID = False
    HAS_BEGIN_TIME = False
    HAS_END_TIME = False
    DO_WATCH_MODE = False
    DO_NEWSCG = False

    try:
        opts, args = getopt.getopt(argv, "", ["move_to_converter_by_id=", "move_to_converter",
             "begin_time=", "end_time=",
             "newscg=",
             "watch"])
    except Exception as e:
        template = "Exception {0} occur : {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)

        sys.exit(2)

    for opt, arg in opts:
        if (opt == "--move_to_converter_by_id"):
            upload_id = arg
            DO_MOVE_TO_CONVERTER_BY_ID = True
        elif (opt == "--move_to_converter"):
            DO_MOVE_TO_CONVERTER = True
        elif (opt == "--newscg"):
            DO_NEWSCG = True
            source = arg
        elif (opt == "--begin_time"):
            HAS_BEGIN_TIME = True
            begin_time = arg
        elif (opt == "--end_time"):
            end_time = arg
            HAS_END_TIME = True
        elif (opt == "--watch"):
            DO_WATCH_MODE = True


    if (DO_MOVE_TO_CONVERTER):
        if (HAS_BEGIN_TIME and HAS_END_TIME):
            if (DO_WATCH_MODE):
                while True:
                    move_to_converter(begin_time, end_time)
                    template = "{0} Sleeping ..."
                    timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
                    msg = template.format(timestamp)
                    print(msg)
                    time.sleep(600)
                    template = "{0} Resume"
                    timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
                    msg = template.format(timestamp)
                    print(msg)
            else:
                move_to_converter(begin_time, end_time)
        else:
            if (DO_WATCH_MODE):
                while True:
                    move_to_converter()
                    template = "{0} Sleeping ..."
                    timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
                    msg = template.format(timestamp)
                    print(msg)
                    time.sleep(600)
                    template = "{0} Resume"
                    timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
                    msg = template.format(timestamp)
                    print(msg)
            else:
                move_to_converter()

    elif (DO_MOVE_TO_CONVERTER_BY_ID):
        move_to_converter_by_id(upload_id)
    
    elif (DO_NEWSCG):
        newscg(source)


if (__name__ == "__main__"):
    main(sys.argv[1:])